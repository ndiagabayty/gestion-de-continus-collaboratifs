<?php

// methode permettant de verifier l'existance d'une constante
if (!function_exists('no_empty')) {

	function no_empty($fields= []){


        if (count($fields)!=0) {

        	foreach ($fields as $field) {
				//si un champ est vide , on return false
				if (empty($_POST[$field]) || trim($_POST[$field])=="") {
					 
					 return false;
				}
		    }
		    // si on parcourt tous les champs et aucun champs n'est vide on return true
		    return true;

           // on parcours tous les champs du tableau
	    }
		

	}
       
   
}
 if (!function_exists('is_already_in_use')) {

 	 function is_already_in_use($field,$value,$table){
          global $db;

          $query = $db->prepare("SELECT id FROM $table WHERE $field=?");
          $query->execute([$value]);

          $result=$query->rowCount();
          $query->closeCursor();

          return $result;
 	 }
 	
 }

 if (!function_exists('set_flash')) {
 	
 	function set_flash($message,$type='info'){
 		$_SESSION['notification']['message']=$message;
 		$_SESSION['notification']['type']=$type;
 	}
 }
// Permettant de rediger l'utilsateur vers une autre page en entree
 if (!function_exists('redirection')) {
 	
 	function redirection($page){
 		header('Location:' .$page);
 		exit(); 
 	}
 }
 // garder les infos en session des au'on trouve une erreur
 if (!function_exists('garder_infos_saisis')) {
 	
 	function garder_infos_saisis(){
 		foreach ($_POST as $key => $value) {
 			if (strpos($key, 'password')==false) {
 				$_SESSION['input'][$key]=$value;
 			}
 		}
 	}
 }
 
 // fonction permettant de recuper les infos en session des qu'on trouve une erreur
 if (!function_exists('recupere_infos_saisis')) {
 	
 	function recupere_infos_saisis($key){
 		if (!empty($_SESSION['input'][$key])) {
 			return echappe($_SESSION['input'][$key]);
 		}else{
 			return null;
 		}
 		
 	}
 }
 // fonction permettant de supprimer les infos en session 
 if (!function_exists('supprimer_les_donnees')) {
 	
 	function supprimer_les_donnees(){
 		if (isset($_SESSION['input'])) {
 			 $_SESSION['input'] =[];
 		
 		}
 		
 	}
 }

 // fonction permettant d'echapper les infos que l'utilisateur a saisi 
 if (!function_exists('echappe')) {
 	
 	function echappe($string){
 		// si la chaine est definie
 		if ($string) {
 			return htmlspecialchars($string);
 			//return htmlentities($string,ENT_QUOTES,UTF-8,false);
 		}
 		
 		
 	}
 }

?>