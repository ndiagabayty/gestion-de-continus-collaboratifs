
<?php $tilte='Inscription';?>


<?php include("partials/_header.php"); ?>
    
    
    <div class="main-content">
         
         <div class="container">

          <h1 class="lead">Devenez deja present membre!</h1>

          <?php
                    include("partials/_error.php");
          ?>

          <form class="well col-md-6" method="post" action="">

            <!--  name field  -->
            <div class="form-group">
              <label class="control-label" for="name">Nom:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('name') ?>" type="text" name="name" id="name" required="required">
            </div>

             <!-- pseudo field  -->
            <div class="form-group">
              <label class="control-label" for="pseudo">Pseudo:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('pseudo') ?>" type="text" name="pseudo" id="pseudo" required="required">
            </div>

             <!-- email field -->
            <div class="form-group">
              <label class="control-label" for="email">Adresse Email:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('email') ?>" type="email" name="email" id="email" required="required">
            </div>

              <!-- password field  -->
            <div class="form-group">
              <label class="control-label" for="password">Mot de Passe:</label>
              <input class="form-control" type="password" name="password" id="password" required="required">
            </div>

             <!-- Fconfirmation field  -->
            <div class="form-group">
              <label class="control-label" for="password_confirm">Confirmer votre mot de passe:</label>
              <input class="form-control" type="password" name="password_confirm" id="password_confirm" required="required">
            </div>
            
            <input class="btn btn-primary" type="submit" name="register" value="Inscription">

          </form>

         </div>

    </div>
    

     <?php include("partials/_footer.php"); ?>