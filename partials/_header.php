
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="reseau social pour developpeur">
    <meta name="author" content="NDIAGA GUEYE">
    
    <title>
          <?= isset($title)? $title.'-'.WEBSITE_NAME:
            WEBSITE_NAME.'simple rapide efficase'; 
            
           ?>
    </title>
      <link href="assets/css/bootstrap.min.css" rel="stylesheet"> 
      <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    
  </head>

  <body>

  <?php include("partials/_nav.php"); ?>
  <?php include("partials/_flash.php"); ?>