<?php
   // Pour pouvoir utliser $_SESSION
   session_start();
   require("config/database.php");
   require("includes/functions.php");
   require("includes/constants.php");
  // le formulaire a ete soumis
   if (isset($_POST['register'])) {

   	 // si tous les champs ont ete remplies
   	if (no_empty(['name','pseudo','email','password','password_confirm']) )
    {
   		$errors =[]; // tableau contenant l4ensemble des erreurs

   		extract($_POST); // permettant d'acceder a tous les elememts

   		if (mb_strlen($pseudo) < 3 ){
   			 $errors[] ="Pseudo trop court (Minimum 3 caracteres)!";
   		}
   		if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
   			$errors[]="Adresse email invalide!";
   		}
   		if (mb_strlen($password) < 6 ){
   			 $errors[] ="Mot de passe trop court (Minimum 6 caracteres)!";
   		} else {
   			if ($password!=$password_confirm) {
   				$errors[]="Vos deux mots de passes ne concordent pas!";
   				
   			}
   		}

   		if (is_already_in_use('pseudo',$pseudo,'users')) {
   			$errors[]="Pseudo deja utilise!";
   		}
        if (is_already_in_use('pseudo',$email,'users')) {
   			$errors[]="Adresse-Email deja utilise!";
   		}
        

        if (count($errors)==0) {

        	// Envoi d'un mail d'actvation
        	$to=$email;
        	$subject=WEBSITE_NAME.'-ACTIVATION DE COMPTE';
          $token=sha1($pseudo.$email.$password);
            
            // garder les infos en memoire tempon
            ob_start();
            require("template/emails/activation.tmpl.php");
            $content= ob_get_clean();

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            //ini_set("SMTP","smtp.gmail.com"); 
            //ini_set("smtp_port","25");

            mail($to,$subject,$content,$headers);
            

            set_flash("Mail d'activation envoye!",'success');
            redirection('index.php');

        }else{
          garder_infos_saisis();
        }

   	} else {
   		 $errors ="Veuillez remplir tous les champs!";
       // garder les infos en session des au'on trouve une erreur
       garder_infos_saisis();
   	}

   	
   } else{
     // permettant de nettoyer les donnees garder en session
     supprimer_les_donnees();
   }

?>




<?php

    require("views/register.views.php");
?>
